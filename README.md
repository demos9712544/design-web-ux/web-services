# Web Services 

🚀 I want to share my latest project with you! ✨


I have just finished the responsive web design using Figma, HTML, CSS3 and media queries, the website adapts to mobile devices, tablets, laptops and desktop computers with custom components and variants. 📱 The goal? Create an intuitive application to explore various IT services.

I hope you like it and any feedback is welcome.


## Documentation

1- Clone Project.
2- Open with Live server extension.

URL DEPLOY

[Website](https://web-services-figma.netlify.app/)


FIGMA DESIGN

[Sketch in Figma](https://www.figma.com/design/rHHESA9BuN3qtUaBFJHbWT/Web-agency?node-id=0-1&t=M2Zfx6f1KdRImo2e-1)

Simulator Iphone 15

[Open Simulator](https://www.figma.com/proto/rHHESA9BuN3qtUaBFJHbWT/Web-agency?node-id=78-2083&t=M2Zfx6f1KdRImo2e-0&scaling=scale-down&content-scaling=fixed&page-id=0%3A1)